## wasm-rust tutorial, stripped of NodeJS stuff

What I want is starting with a minimal Rust Webassembly environment,
and build up from there.  The [book's
tutorial](https://rustwasm.github.io/docs/book/game-of-life/introduction.html)
is nice but uses a NodeJS-based webpack that tends to get in people's way.

This minimal adaptation produces simple artifacts that can be served
off any web server, like Gitlab Pages for official publication of your
app, a simple `python3 -m http.server 8000` for local testing, or any
alternative of your choice.

## See in action

This is deployed using Gitlab CI to be published from [Gitlab
Pages](https://ydirson.gitlab.io/test-rust-wasm/).

## Digging

Start with:
- [the CI rules](/.gitlab-ci.yml) (this is a multi-step build...)
- [my rust-wasm blog series](https://ydirson.gitlab.io/the-floss-cook/rust/2023/12/01/rust-wasm-first-contact.html)
- [the tutorial itself](https://rustwasm.github.io/docs/book/game-of-life/introduction.html)

## License

Licensed under either of

* Apache License, Version 2.0, ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
* MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

### Contribution

Unless you explicitly state otherwise, any contribution intentionally
submitted for inclusion in the work by you, as defined in the Apache-2.0
license, shall be dual licensed as above, without any additional terms or
conditions.
